#include <vector>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Double_Window.H>

// sky widget
class sky : public Fl_Widget {
public:
    sky(int W, int H);
    void draw();
    void add_star(int X, int Y, int R,Fl_Color C);
    int getNstars();
private:
    std::vector<int> x_pos;
    std::vector<int> y_pos;
    std::vector<int> radius;
    std::vector<Fl_Color> colors;
};

// -------------- UNUSED ------------ //

// Star widgets
class circle : Fl_Widget {
public:
    circle(int X, int Y, int r);
    void draw();
};
