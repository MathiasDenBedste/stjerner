#include <vector>
#include <FL/Fl.H>
#include <FL/fl_draw.H>
#include <FL/Fl_Double_Window.H>
#include "simple_stars.h"

sky::sky(int W, int H) : Fl_Widget(0,0,W,H,0) {}

void sky::draw(){
    for(int k = 0; k < this->getNstars(); k++){
        fl_color(colors[k]);
        fl_pie(x_pos[k],y_pos[k],radius[k],radius[k],0,360);
    }
}

void sky::add_star(int X, int Y, int R,Fl_Color C){
    x_pos.push_back(X);
    y_pos.push_back(Y);
    radius.push_back(R);
    colors.push_back(C);
}

int sky::getNstars(){
    return x_pos.size();
}

// -------------- UNUSED ------------ //

circle::circle(int X, int Y, int r) : Fl_Widget(X,Y,r,r,0) {}

void circle::draw(){
    fl_color(FL_WHITE);
    fl_pie(x(),y(),w(),h(),0,360);
}
